#!/bin/sh

for i in 1 2 4 8 16 32 64; do
   t0=`date +%s.%N`
   OMP_NUM_THREADS=$i ./genetic_algorithm 1>/dev/null
   t1=`date +%s.%N`
   t=`echo $t0 $t1 | awk '{printf("%f", $2-$1)}'`
   echo $i, $t
done